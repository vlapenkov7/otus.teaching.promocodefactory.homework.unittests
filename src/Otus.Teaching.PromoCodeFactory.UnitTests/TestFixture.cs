﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.WebHost;
using StateMachine.Compose;

namespace StateMachine.BusinessLogic.Managers.UnitTests_Demo
{
    public class TestFixture : IDisposable
    {
        public IServiceProvider ServiceProvider { get; set; }

        public IServiceCollection ServiceCollection { get; set; }

        /// <summary>
        /// Выполняется перед запуском тестов
        /// </summary>
        public TestFixture()
        {
            var builder = new ConfigurationBuilder();
            var configuration = builder.Build();
            ServiceCollection = Configuration.GetServiceCollection(configuration, "Tests");
            var serviceProvider = ServiceCollection
                .BuildServiceProvider();
            ServiceProvider = serviceProvider;
            new EfDbInitializer(serviceProvider.GetRequiredService<DataContext>()).InitializeDb();
        }


        public void Dispose()
        {
        }
    }
}
