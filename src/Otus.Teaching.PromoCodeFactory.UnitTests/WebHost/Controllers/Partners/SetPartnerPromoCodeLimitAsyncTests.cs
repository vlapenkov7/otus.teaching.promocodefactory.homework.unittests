﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using StateMachine.BusinessLogic.Managers.UnitTests_Demo;
using System;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests : IClassFixture<TestFixture>
    {

        private IRepository<Partner> _partnersRepository;


        public SetPartnerPromoCodeLimitAsyncTests(TestFixture testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
            _partnersRepository = serviceProvider.GetService<IRepository<Partner>>();

        }

        /// <summary>
        /// Партнер не найден
        /// </summary>        
        [Fact]
        public async Task SetLimitWithNonExistingPartner_Returns_NotFound()
        {
            var fakepartnerGuid = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df1");

            var controller = new PartnersController(_partnersRepository);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 10,
                EndDate = DateTime.Now
            };
            var result = await controller.SetPartnerPromoCodeLimitAsync(fakepartnerGuid, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();

        }

        /// <summary>
        /// Партнер не активен
        /// </summary>

        [Fact]
        public async Task SetLimitWithNonActivePartner_Returns_NotFound()
        {
            var inActivepartnerGuid = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43");

            var controller = new PartnersController(_partnersRepository);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 10,
                EndDate = DateTime.Now
            };
            var result = await controller.SetPartnerPromoCodeLimitAsync(inActivepartnerGuid, request);

            // Assert
            var subject = result.Should().BeAssignableTo<BadRequestObjectResult>().Subject;

            subject.Value.Should().Be("Данный партнер не активен");

        }


        /// <summary>
        /// Лимит отрицательный
        /// </summary>
        [Fact]
        public async Task SetLimitWithNegativeLimit_Returns_BadRequest()
        {
            var partnerGuid = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

            var controller = new PartnersController(_partnersRepository);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = -10
            };
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerGuid, request);

            // Assert
            var subject = result.Should().BeAssignableTo<BadRequestObjectResult>().Subject;

            subject.Value.Should().Be("Лимит должен быть больше 0");
        }


        /*  Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
            При установке лимита нужно отключить предыдущий лимит;
            Нужно убедиться, что сохранили новый лимит в базу данных (это нужно проверить Unit-тестом);
        */
        [Fact]

        public async Task SetLimitWithPreviousActiveLimitNumberIssuedPromoCodes_Should_Changed()
        {
            var partnerGuid = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8");

            var controller = new PartnersController(_partnersRepository);

            var request = new SetPartnerPromoCodeLimitRequest
            {
                Limit = 10,
                EndDate = DateTime.Now
            };
            var result = await controller.SetPartnerPromoCodeLimitAsync(partnerGuid, request);

            // Assert
            var subject = result.Should().BeOfType<CreatedAtActionResult>().Subject;


            var partnerFound = await _partnersRepository.GetByIdAsync(partnerGuid);

            partnerFound.Should().NotBeNull();

            //Обнулили число промокодов
            partnerFound.NumberIssuedPromoCodes.Should().Be(0);

            //Новый лимит добавился
            partnerFound.PartnerLimits.Count().Should().Be(2);

            //Первая запись cancelDate установился
            partnerFound.PartnerLimits.First().CancelDate.Should().NotBeNull();

            //Новая запись cancelDate пустой
            partnerFound.PartnerLimits.Last().CancelDate.Should().BeNull();

            //Новый лимит добавился
            partnerFound.PartnerLimits.Count().Should().Be(2);


        }
    }
}