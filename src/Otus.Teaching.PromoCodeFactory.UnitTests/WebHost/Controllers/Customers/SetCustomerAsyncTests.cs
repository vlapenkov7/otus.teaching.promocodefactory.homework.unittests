﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using StateMachine.BusinessLogic.Managers.UnitTests_Demo;
using System;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Customers
{

    public class CustomersAsyncTests : IClassFixture<TestFixture>
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;



        public CustomersAsyncTests(TestFixture testFixture)
        {
            var serviceProvider = testFixture.ServiceProvider;
            _customerRepository = serviceProvider.GetService<IRepository<Customer>>();
            _preferenceRepository = serviceProvider.GetService<IRepository<Preference>>();

        }

        //[Fact]
        //public async Task Get_Should_Return_One_Element()
        //{

        //    var result = await _customerRepository.GetAllAsync();

        //    Assert.Equal(1, result.Count());
        //}

        [Fact]
        public async Task Create_Should_Add_One_Element()
        {
            var service = new ClientService(_customerRepository, _preferenceRepository);

            var request = new CreateOrEditCustomerRequest
            {
                FirstName = "First",
                LastName = "First",
                Email = "test@werm.ru"
            };
            Guid customerId = await service.CreateCustomerAsync(request);

            var customerFound = await _customerRepository.GetByIdAsync(customerId);

            Assert.NotNull(customerFound);

            Assert.Equal("First", customerFound.FirstName);
            Assert.Equal("First", customerFound.LastName);
        }

        // Вот такая штука не пройдет, т.к. одна in-memory db
        //[Fact]
        //public async Task Create_Should_Add_Two_Elements()
        //{
        //    var service = new ClientService(_customerRepository, _preferenceRepository);

        //    var request = new CreateOrEditCustomerRequest
        //    {
        //        FirstName = "First",
        //        LastName = "First",
        //        Email = "test@werm.ru"
        //    };
        //    await service.CreateCustomerAsync(request);

        //    await service.CreateCustomerAsync(request);

        //    var result = await _customerRepository.GetAllAsync();

        //    Assert.Equal(3, result.Count());
        //}


        [Fact]
        public async Task Update_Should_Add_Two_Elements()
        {
            Guid customerGuidInDatabase = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");

            var service = new ClientService(_customerRepository, _preferenceRepository);

            var request = new CreateOrEditCustomerRequest
            {
                FirstName = "Second",
                LastName = "Second",
                Email = "test@werm.ru"
            };
            await service.UpdateCustomerAsync(customerGuidInDatabase, request);


            var result = await _customerRepository.GetByIdAsync(customerGuidInDatabase);

            Assert.Equal("Second", result.FirstName);

        }
    }
}