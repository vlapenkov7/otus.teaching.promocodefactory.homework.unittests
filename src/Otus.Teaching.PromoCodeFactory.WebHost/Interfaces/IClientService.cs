﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Interfaces
{
    public interface IClientService
    {
        Task<Guid> CreateCustomerAsync(CreateOrEditCustomerRequest request);

        Task UpdateCustomerAsync(Guid id, CreateOrEditCustomerRequest request);
    }
}
