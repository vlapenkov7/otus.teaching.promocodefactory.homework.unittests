﻿//using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Interfaces;
using Otus.Teaching.PromoCodeFactory.WebHost.Services;

namespace StateMachine.Compose
{
    public static class Configuration
    {

        public static IServiceCollection GetServiceCollection(IConfiguration configuration, string serviceName, IServiceCollection serviceCollection = null)
        {
            if (serviceCollection == null)
            {
                serviceCollection = new ServiceCollection();
                serviceCollection.ConfigureInMemoryContext();
            }
            else
            {
                serviceCollection.ConfigureDbContext();
            }
            serviceCollection
                .AddSingleton(configuration)
                .AddSingleton((IConfiguration)configuration);

            //   .ConfigureAutomapper()
            serviceCollection.ConfigureAllRepositories()
                .ConfigureAllBusinessServices()
                .AddLogging(builder =>
                {
                    builder.ClearProviders();
                    builder.AddConfiguration(configuration.GetSection("Logging"));
                    builder
                        .AddFilter("Microsoft", LogLevel.Warning)
                        .AddFilter("System", LogLevel.Warning);
                })
                .AddMemoryCache();
            return serviceCollection;
        }

        public static IServiceCollection ConfigureInMemoryContext(this IServiceCollection services)
        {
            var serviceProvider = new ServiceCollection()
                .AddEntityFrameworkInMemoryDatabase()
                .BuildServiceProvider();
            services.AddDbContext<DataContext>(options =>
            {
                options.UseInMemoryDatabase("InMemoryDb", builder => { });
                options.UseInternalServiceProvider(serviceProvider);
            });
            services.AddTransient<DbContext, DataContext>();
            return services;
        }

        public static IServiceCollection ConfigureDbContext(this IServiceCollection services)
        {
            services.AddDbContext<DataContext>(x =>
            {
                x.UseSqlite("Filename=PromoCodeFactoryDb.sqlite");
                //x.UseNpgsql(Configuration.GetConnectionString("PromoCodeFactoryDb"));
                x.UseSnakeCaseNamingConvention();
                x.UseLazyLoadingProxies();
            });
            return services;
        }


        //public static IServiceCollection ConfigureAutomapper(this IServiceCollection services) => services
        //    .AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));

        public static IServiceCollection ConfigureAllRepositories(this IServiceCollection services) => services
             .AddScoped(typeof(IRepository<>), typeof(EfRepository<>)).
                AddScoped<IDbInitializer, EfDbInitializer>();

        private static IServiceCollection ConfigureAllBusinessServices(this IServiceCollection services) => services
            .AddTransient<IClientService, ClientService>();
        //.AddTransient<ILotManager, LotManager>()
        //.AddTransient<ILotDocumentService, LotDocumentService>();

        //private static MapperConfiguration GetMapperConfiguration()
        //{
        //    var configuration = new MapperConfiguration(cfg =>
        //    {
        //        cfg.AddProfile<LotManagerMapProfiles>();
        //    });
        //    configuration.AssertConfigurationIsValid();
        //    return configuration;
        //}
    }
}
